terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.40"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
  storage_use_azuread = true
  skip_provider_registration = true
}
