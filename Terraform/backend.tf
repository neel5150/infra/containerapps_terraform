terraform {
  backend "azurerm" {
    resource_group_name  = "qps_poc"
    storage_account_name = "containertfstorage"
    container_name       = "terraform-state"
    key                  = "capp.terraform.tfstate"
    use_azuread_auth     = true
  }
}