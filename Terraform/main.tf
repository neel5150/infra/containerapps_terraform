/*resource "azurerm_storage_account" "state_storage" {
  name = var.storage_account_name
  location = var.location
  resource_group_name = var.resource_group
  account_tier = var.account_tier
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "state_container" {
  name = var.state_container_name
  storage_account_name = azurerm_storage_account.state_storage.name
}*/

data "azurerm_container_app_environment" "containerapp_environment" {
  name                   = var.containerapp_environment_name
  resource_group_name    = var.resource_group
}

locals {
  image = "${var.acr}/${var.image_version}"
}

resource "azurerm_container_app" "container_app" {
  name                     = var.containerapp_name
  container_app_environment_id = data.azurerm_container_app_environment.containerapp_environment.id
  resource_group_name        = var.resource_group
  revision_mode             = var.revision_mode

  template {
    container {
      name  = var.containerapp_name
      image = local.image
      cpu    = 0.25
      memory = "0.5Gi"
    }
  }
}

/*terraform {
  backend "azurerm" {
      resource_group_name  = "qps_poc"
      storage_account_name = "containertfstorage"
      container_name       = "terraform-state"
      key                  = "terraform.tfstate"
  }
}*/


