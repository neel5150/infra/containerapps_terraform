variable "storage_account_name" {
  type = string
}

variable "account_tier" {
  type = string
}

variable "location" {
  type = string
}

variable "account_replication_type" {
  type = string
}

variable "containerapp_name" {
  type = string
}

variable "state_container_name" {
  type = string
}

variable "containerapp_environment_name" {
  type = string
}

variable "resource_group" {
  type = string
}

variable "contianerapp_name" {
  type = string
}

variable "revision_mode" {
  type = string
}

variable "acr" {
  type = string
}

variable "image_version" {
  type = string
}