FROM hashicorp/terraform:light AS base

# Install dependencies required for Azure CLI and Terraform (if not already present)
RUN apk add --no-cache \
    python3 \
    py3-pip \
    curl \
    ca-certificates \
    bash-completion \
    jq \
    gcc \
    python3-dev \
    musl-dev \
    linux-headers

# Install Azure CLI using pip (avoids unnecessary package updates)
RUN pip3 install --upgrade azure-cli

# Set working directory (optional, adjust based on your project structure)
WORKDIR /app

# Set entrypoint
ENTRYPOINT ["/usr/bin/env"]